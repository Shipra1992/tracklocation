//
//  AppDelegate.h
//  TrackLocation
//
//  Created by Shipra Gupta on 10/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RKManagedObjectStore *managedObjectStore;
@end

