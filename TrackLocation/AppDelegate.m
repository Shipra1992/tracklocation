//
//  AppDelegate.m
//  TrackLocation
//
//  Created by Shipra Gupta on 10/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "Location.h"

@interface AppDelegate ()

@end

static NSString *applicationID = @"8TNMQWfj0Lg2414nszXcyYaB8NhIbIxZfZwJlpkj";
static NSString *clientKey = @"es95GGDElmq9KePCVZX5RPMbT0E36ccvpKwYfWIp";
static NSString *kRESTapiKey = @"KVJqUfWLMHTwKD6EWvzmH4mKcDY0TcG4DmNdBtLl";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if (![CLLocationManager locationServicesEnabled]) {
        // location services is disabled, alert user
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services is Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled."
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
    
    //Connection to parse
    [Parse setApplicationId:applicationID
                  clientKey:clientKey];
    
    //Track statistics around application opens
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [self setupRestKit];
    return YES;
}

- (void)setupRestKit {
    //initiate object Manager, model and store
    
    NSManagedObjectModel *managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] mutableCopy];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    [self createObjectManager];
    
    // Initialize the Core Data stack
    [managedObjectStore createPersistentStoreCoordinator];
    NSError *error = nil;
    NSPersistentStore __unused *persistentStore = [managedObjectStore addInMemoryPersistentStore:&error];
    NSAssert(persistentStore, @"Failed to add persistent store: %@", error);
    [managedObjectStore createManagedObjectContexts];
    
    // Set the default store shared instance
    [RKManagedObjectStore setDefaultStore:managedObjectStore];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    // Set it Globally
    [RKEntityMapping setPreferredDateFormatter:dateFormatter];

}

//- (NSManagedObjectModel *)managedObjectModel {
//    if (!_managedObjectModel) {
//        //  Get the mom URL from ShaleSDK.bundle
//        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"ShaleSDK" ofType:@"bundle"];
//        NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
//        NSURL *bundleMomURL = [bundle URLForResource:storeName withExtension:@"momd"];
//        //  Get the mom mom URL from the current bundle
//        NSURL *momURL = [[NSBundle bundleForClass:[self class]] URLForResource:storeName withExtension:@"momd"];
//        
//        //  Initialize managed object model and persistent store coordinator
//        NSManagedObjectModel *bundleMom = [[NSManagedObjectModel alloc] initWithContentsOfURL:bundleMomURL];
//        //  If there's no bundle mom, then use the one in the current bundle
//        _managedObjectModel = bundleMom ? bundleMom : [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
//    }
//    
//    return _managedObjectModel;
//}

- (void)createObjectManager {
    NSURL *baseURL = [NSURL URLWithString:@"https://api.parse.com/1/"];
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    objectManager.managedObjectStore = [RKManagedObjectStore defaultStore];
    [objectManager.HTTPClient setParameterEncoding:AFJSONParameterEncoding];
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    [objectManager.HTTPClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [objectManager.HTTPClient setDefaultHeader:@"X-Parse-Application-Id" value:applicationID];
    [objectManager.HTTPClient setDefaultHeader:@"X-Parse-REST-API-Key" value:kRESTapiKey];
    [RKObjectManager setSharedManager:objectManager];
}

@end
