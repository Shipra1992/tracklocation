//
//  PlotLocationMapViewController.h
//  TrackLocation
//
//  Created by Shipra Gupta on 27/11/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawRouteMapViewController : UIViewController
@property (nonatomic, strong) NSArray *locations;
@end
