//
//  PlotLocationMapViewController.m
//  TrackLocation
//
//  Created by Shipra Gupta on 27/11/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "DrawRouteMapViewController.h"
#import <MapKit/MapKit.h>
#import <RestKit/RestKit.h>
#import "Location.h"
#import "HCSAnnotation.h"

@interface DrawRouteMapViewController () <MKMapViewDelegate, CLLocationManagerDelegate>
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;

@end

@implementation DrawRouteMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView = [[MKMapView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.mapView];
    self.mapView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    if (self.locations) {
    //[self drawRoute];
        [self drawPolyline];
    }
}

- (void)drawPolyline {
    CLLocationCoordinate2D *coordinateArray = malloc(sizeof(CLLocationCoordinate2D) * self.locations.count);
    int caIndex = 0;
    CLLocationCoordinate2D coordinate;
    for (Location *loc in self.locations) {
        coordinate.latitude = loc.latitude.doubleValue;
        coordinate.longitude = loc.longitude.doubleValue;
        coordinateArray[caIndex] = coordinate;
        caIndex++;
        [self.mapView addAnnotation:[[HCSAnnotation alloc] initWithLocation:coordinate title:[NSString stringWithFormat:@"%d",caIndex] subtitle:@""]];
    }
    [self zoomToFitAnnotations];
    
    MKPolyline *lines = [MKPolyline polylineWithCoordinates:coordinateArray
                                                      count:self.locations.count];
    free(coordinateArray);
    
    [self.mapView addOverlay:lines];

}

- (void)drawRoute{
    for (int i = 0 ; i < self.locations.count - 1; i++) {
        Location *src = [self.locations objectAtIndex:i] ;
        Location *dest = [self.locations objectAtIndex:i+1];
        CLLocationCoordinate2D srcCoord, destCoord;
        srcCoord.latitude = src.latitude.doubleValue;
        srcCoord.longitude = src.longitude.doubleValue;
        destCoord.latitude = dest.latitude.doubleValue;
        destCoord.longitude = dest.longitude.doubleValue;
        [self.mapView addAnnotation:[[HCSAnnotation alloc] initWithLocation:srcCoord title:[NSString stringWithFormat:@"%d",i+1] subtitle:@""]];
        [self.mapView addAnnotation:[[HCSAnnotation alloc] initWithLocation:destCoord title:[NSString stringWithFormat:@"%d",i+2] subtitle:@""]];
        [self drawRouteFromSource:srcCoord toDestination:destCoord];
   }
    [self zoomToFitAnnotations];
}

- (void)drawRouteFromSource:(CLLocationCoordinate2D)sourceCoord toDestination:(CLLocationCoordinate2D)destCoord {
    
    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:sourceCoord addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:@""];
    
    MKPlacemark *destination= [[MKPlacemark alloc]initWithCoordinate:destCoord addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:@""];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeWalking];
    request.requestsAlternateRoutes = YES;
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSLog(@"response = %@",response);
        NSArray *arrRoutes = [response routes];
        for (MKRoute *rout in arrRoutes) {
            [self.mapView addOverlay:rout.polyline level:MKOverlayLevelAboveLabels];
        }
    }];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc]initWithPolyline:(MKPolyline*)overlay] ;
        renderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        renderer.lineWidth = 5;
        return renderer;
}

- (NSNumberFormatter *)numberFormatter {
    static NSNumberFormatter* _sharedNumberFormatter;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedNumberFormatter = [[NSNumberFormatter alloc] init];
    });
    return _sharedNumberFormatter;
}

- (void)zoomToFitAnnotations {
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in self.mapView.annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x , annotationPoint.y , 10.0, 10.0);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    double inset = -zoomRect.size.width * 0.1;
    [self.mapView setVisibleMapRect:MKMapRectInset(zoomRect, inset, inset) animated:YES];
}

@end
