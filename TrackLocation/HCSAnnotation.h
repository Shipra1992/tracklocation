//
//  HCSAnnotation.h
//  TrackLocation
//
//  Created by Shipra Gupta on 27/11/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface HCSAnnotation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
- (id)initWithLocation:(CLLocationCoordinate2D)coord;
- (id)initWithLocation:(CLLocationCoordinate2D)coord title:(NSString *)title subtitle:(NSString *)subtitle;

@end
