//
//  HCSAnnotation.m
//  TrackLocation
//
//  Created by Shipra Gupta on 27/11/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "HCSAnnotation.h"

@implementation HCSAnnotation
@synthesize title = _title, subtitle = _subtitle;

- (id)initWithLocation:(CLLocationCoordinate2D)coord {
    self = [super init];
    if (self) {
        _coordinate = coord;
        _title = @"title";
        _subtitle = @"subtitle";
    }
    
    return self;
}

- (id)initWithLocation:(CLLocationCoordinate2D)coord title:(NSString *)title subtitle:(NSString *)subtitle {
    self = [super init];
    if (self) {
        _coordinate = coord;
        _title = title;
        _subtitle = subtitle;
    }
    return self;
}

@end
