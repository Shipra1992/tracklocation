//
//  Location.h
//  TrackLocation
//
//  Created by Shipra Gupta on 12/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic) NSDate * timestamp;
@property (nonatomic, retain) NSString * horAccuracy;
@property (nonatomic, retain) NSString * verAccuracy;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * routeId;

@end
