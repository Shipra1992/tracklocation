//
//  Location.m
//  TrackLocation
//
//  Created by Shipra Gupta on 12/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "Location.h"

@implementation Location

@dynamic latitude;
@dynamic longitude;
@dynamic timestamp;
@dynamic horAccuracy;
@dynamic verAccuracy;

@dynamic uuid;
@dynamic routeId;

@end
