//
//  StoreManager.h
//  TrackLocation
//
//  Created by Shipra Gupta on 12/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LocationDocument : NSObject

@property (nonatomic, strong) UIManagedDocument *document;
@property (nonatomic, strong) NSURL *documentUrl;

+ (LocationDocument *)documentManager;
- (UIManagedDocument *)managedDocument;

@end
