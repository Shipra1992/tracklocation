//
//  StoreManager.m
//  TrackLocation
//
//  Created by Shipra Gupta on 12/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "LocationDocument.h"

@implementation LocationDocument

+ (LocationDocument *)documentManager
{
    static LocationDocument* _sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (UIManagedDocument *)managedDocument {
    if(!self.document) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSURL *documentsDirectory = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
        NSString *documentName = @"TrackLocation";
        NSURL *url = [documentsDirectory URLByAppendingPathComponent:documentName];
        self.document = [[UIManagedDocument alloc]initWithFileURL:url];
        self.documentUrl = url;
    }
    return self.document;
}


@end
