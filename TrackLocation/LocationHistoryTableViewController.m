//
//  LocationHistoryTableViewController.m
//  TrackLocation
//
//  Created by Shipra Gupta on 21/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "LocationHistoryTableViewController.h"
#import "PlotLocationMapViewController.h"
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import <Parse/Parse.h>
#import "RouteObject.h"
#import "LocationObject.h"
#import "TTTURLRequestFormatter.h"

@interface LocationHistoryTableViewController ()
@property (nonatomic, strong) NSArray *locations;
@property (nonatomic, strong) NSArray *routes;
@property (nonatomic, strong) NSString *deviceUUID;
@end

@implementation LocationHistoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.deviceUUID = @"D77633D6-374B-4996-8228-9B2712A1F7E1";
    //[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)loadData {
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    RKObjectMapping *routeMapping = [RKObjectMapping mappingForClass:[RouteObject class]];
    
    [routeMapping addAttributeMappingsFromDictionary:@{
                                                          @"endTime.iso" : @"endTime",
                                                          @"startTime.iso" : @"startTime",
                                                          @"uuid" : @"uuid",
                                                          @"routeId" : @"routeId"
                                                          }];
    
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:routeMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:@"classes/Route"
                                                                                           keyPath:@"results"
                                                                                       statusCodes:statusCodes];
    [manager addResponseDescriptor:responseDescriptor];
    
    NSDictionary *queryParams = @{@"where" : @{ @"uuid" : self.deviceUUID }};
    
    
//    [manager.HTTPClient getPath:@"classes/Route"
//                     parameters:@{
//                                  @"endTime.iso" : @"endTime",
//                                  @"startTime.iso" : @"startTime",
//                                  @"uuid" : @"uuid",
//                                  @"routeId" : @"routeId"
//                                  }
//                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                            NSLog(@"success");
//                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                            NSLog(@"failure");
//                        }];
    
    [manager getObjectsAtPath:@"classes/Route"
                   parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation,
                                                        RKMappingResult *mappingResult) {
                                                  self.routes = mappingResult.array;
                                                  [self.tableView reloadData];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"err");
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.routes count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"locationCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    RouteObject *route = [self.routes objectAtIndex:indexPath.row];
    NSString *title = [NSString stringWithFormat:@"Route : %ld", (long)indexPath.row+1];

    cell.textLabel.text = title;
   
    NSTimeInterval interval = [route.endTime timeIntervalSinceDate:route.startTime];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Duration :%f sec",interval];

    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        if ([[segue identifier] isEqualToString:@"DrawAnnotation"]) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            PlotLocationMapViewController *plmvc = (PlotLocationMapViewController *)segue.destinationViewController;
            plmvc.route = [self.routes objectAtIndex:indexPath.row];
        }
    }
}


@end