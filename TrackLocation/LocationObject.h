//
//  LocationObject.h
//  TrackLocation
//
//  Created by Shipra Gupta on 03/12/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationObject : NSObject

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic) NSDate *timestamp;
@property (nonatomic, strong) NSString *horAccuracy;
@property (nonatomic, strong) NSString *verAccuracy;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *routeId;


@end
