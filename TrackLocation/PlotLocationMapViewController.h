//
//  PlotLocationMapViewController.h
//  TrackLocation
//
//  Created by Shipra Gupta on 28/11/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RouteObject.h"

@interface PlotLocationMapViewController : UIViewController
@property (nonatomic, strong) RouteObject *route;
@end
