//
//  PlotLocationMapViewController.m
//  TrackLocation
//
//  Created by Shipra Gupta on 28/11/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "PlotLocationMapViewController.h"
#import <MapKit/MapKit.h>
#import "HCSAnnotation.h"
#import "LocationObject.h"
#import <RestKit/RestKit.h>

@interface PlotLocationMapViewController () <MKMapViewDelegate, CLLocationManagerDelegate>
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;
@property (nonatomic, strong) NSArray *locations;
@end

@implementation PlotLocationMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView = [[MKMapView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.mapView];
    self.mapView.delegate = self;
    
    [self.mapView removeOverlays:self.mapView.overlays];
    [self loadLocations];
}

- (void)drawPolyline {
    CLLocationCoordinate2D *coordinateArray = malloc(sizeof(CLLocationCoordinate2D) * self.locations.count);
    int caIndex = 0;
    CLLocationCoordinate2D coordinate;
    for (LocationObject *loc in self.locations) {
        coordinate.latitude = loc.latitude.doubleValue;
        coordinate.longitude = loc.longitude.doubleValue;
        coordinateArray[caIndex] = coordinate;
        caIndex++;
        [self.mapView addAnnotation:[[HCSAnnotation alloc] initWithLocation:coordinate title:[NSString stringWithFormat:@"%d",caIndex] subtitle:@""]];
    }
    [self zoomToFitAnnotations];
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    MKPolyline *lines = [MKPolyline polylineWithCoordinates:coordinateArray
                                                      count:self.locations.count];
    free(coordinateArray);
    
    [self.mapView addOverlay:lines];

}

- (void)zoomToFitAnnotations {
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in self.mapView.annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x , annotationPoint.y , 10.0, 10.0);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    double inset = -zoomRect.size.width * 0.1;
    [self.mapView setVisibleMapRect:MKMapRectInset(zoomRect, inset, inset) animated:YES];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay {
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc]initWithPolyline:(MKPolyline*)overlay] ;
    renderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
    renderer.lineWidth = 5;
    return renderer;
}

- (void)loadLocations {
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    RKObjectMapping *locationMapping = [RKObjectMapping mappingForClass:[LocationObject class]];
     //RKEntityMapping *locationMapping = [RKEntityMapping mappingForEntityForName:@"Location" inManagedObjectStore:[RKManagedObjectStore defaultStore]];
    [locationMapping addAttributeMappingsFromDictionary:@{
                                                          @"horAccuracy" : @"horAccuracy",
                                                          @"latitude"    : @"latitude",
                                                          @"longitude"   : @"longitude",
                                                          @"verAccuracy" : @"verAccuracy",
                                                          @"timestamp.iso" : @"timestamp",
                                                          @"uuid" : @"uuid",
                                                          @"routeId" : @"routeId"
                                                          }];
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    
    
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:@"classes/LocationObject"
                                                                                           keyPath:@"results"
                                                                                       statusCodes:statusCodes];
    [manager addResponseDescriptor:responseDescriptor];
    
    NSDictionary *queryParams = @{@"where" : @{ @"uuid" : self.route.uuid ,
                                                @"routeId" : self.route.routeId},
                                  @"order" : @"timestamp"
                                  };
    
    
    [manager getObjectsAtPath:@"classes/LocationObject"
                   parameters:queryParams
                      success:^(RKObjectRequestOperation *operation,
                                RKMappingResult *mappingResult) {
                          self.locations = mappingResult.array;
                      } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          NSLog(@"err");
                      }];
}

- (void)setLocations:(NSArray *)locations {
    _locations = locations;
    if (_locations) {
        [self drawPolyline];
    }
}


- (void)zoomIn:(CLLocationCoordinate2D)coordinate {
    MKCoordinateRegion region;
    region.center = coordinate;
    region.span = MKCoordinateSpanMake(0.1, 0.1);
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
}

- (NSNumberFormatter *)numberFormatter {
    static NSNumberFormatter* _sharedNumberFormatter;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedNumberFormatter = [[NSNumberFormatter alloc] init];
    });
    return _sharedNumberFormatter;
}

@end
