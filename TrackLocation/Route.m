//
//  Route.m
//  TrackLocation
//
//  Created by Shipra Gupta on 01/12/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "Route.h"

@implementation Route

@dynamic routeId;
@dynamic uuid;
@dynamic startTime;
@dynamic endTime;

@end
