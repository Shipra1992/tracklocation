//
//  RouteObject.h
//  TrackLocation
//
//  Created by Shipra Gupta on 03/12/2014.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RouteObject : NSObject

@property (nonatomic, retain) NSString *routeId;
@property (nonatomic, retain) NSString *uuid;
@property (nonatomic, retain) NSDate *startTime;
@property (nonatomic, retain) NSDate *endTime;


@end
