//
//  TrackLocationMapViewController.m
//  TrackLocation
//
//  Created by Shipra Gupta on 10/11/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import "TrackLocationMapViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationDetailViewController.h"
#import <Parse/Parse.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Location.h"
#import "Route.h"
#import <RestKit/RestKit.h>
#import "HCSAnnotation.h"

@interface TrackLocationMapViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) HCSAnnotation *annotation;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *startButton;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) CLPlacemark *placemark;
@property (nonatomic) BOOL zoomIn;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic) BOOL isLocation;
@property (nonatomic, strong) NSString *deviceUUID;
@property (nonatomic, strong) NSString *routeID;
@property (nonatomic, strong) NSDate *startRouteTime;
@property (nonatomic, strong) NSDate *endRouteTime;
@property (nonatomic) BOOL hasRouteStarted;

@end

static NSTimeInterval interval = 200.0;
static const int NUMBER_OF_ITEMS = 100;
typedef void(^postRequestCompletion)(NSSet *sentItems, BOOL success);

@implementation TrackLocationMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView = [[MKMapView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.mapView];
    [self setResponseAndRequestDescriptors];
    self.deviceUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [self sendLocationsOnInterval:&interval];
    [self sendRoutesOnInterval:&interval];

}

- (IBAction)startTrackingLocation:(id)sender {
    self.startButton.enabled = NO;
    UIBarButtonItem *stopButton = [[UIBarButtonItem alloc] initWithTitle:@"Stop"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(stopTrackingLocation)];
    [self.navigationItem setRightBarButtonItem:stopButton animated:YES];

    _locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.distanceFilter = 10.0f;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    self.routeID = [self generateUUID];
    
     [self.locationManager startUpdatingLocation];
}

- (void)stopTrackingLocation {
    self.endRouteTime = [NSDate date];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self zoomInZoomOut];
    
     [UIView animateWithDuration:0.6f animations:^(void) {
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
        self.startButton.enabled = YES;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.locationManager stopUpdatingLocation];
        }
    }];
    [self saveRouteInCoreData];
    self.routeID = nil;
}

- (void)saveRouteInCoreData {
    Route *route = [NSEntityDescription insertNewObjectForEntityForName:@"Route" inManagedObjectContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext];
    route.uuid = self.deviceUUID;
    route.routeId = self.routeID;
    route.endTime = self.endRouteTime;
    route.startTime = self.startRouteTime;
}

- (NSString *)generateUUID {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef str = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    NSString *string = (__bridge NSString *)str;
    NSMutableString *result = [NSMutableString stringWithString:string];
    [result replaceOccurrencesOfString:@"-" withString:@"" options:NSLiteralSearch
                                 range:NSMakeRange(0, [result length]) ];
    
    return result;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = [locations lastObject];
    [self.geocoder reverseGeocodeLocation:self.location completionHandler:^(NSArray *placemarks, NSError *error) {
        //NSLog(@"%@", placemarks);
        self.placemark = [placemarks objectAtIndex:0];
        self.location = newLocation;
        if (newLocation) {
            [self saveLocationInCoreData:newLocation];
        }
        
        if (!self.hasRouteStarted) {
            self.hasRouteStarted = YES;
            self.startRouteTime = newLocation.timestamp;
        }
    }];
}

- (NSTimer *)sendLocationsOnInterval:(NSTimeInterval *)ti {
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:*ti target:self selector:@selector(sendItem:) userInfo:[Location class] repeats:YES];
    return timer;
}

- (NSTimer *)sendRoutesOnInterval:(NSTimeInterval *)ti {
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:*ti target:self selector:@selector(sendItem:) userInfo:[Route class] repeats:YES];
    return timer;
}

- (void)fetchItems:(Class)class withCompletionBlock:(void(^)(NSSet *items, NSError *error))completion {
    NSError *error;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(class)];
    NSArray *managedObjects = [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext executeFetchRequest:fetchRequest
         
                                                                                                               error:&error];
    if (managedObjects.count > 0) {
    NSMutableSet *items = [NSMutableSet set];
    for (NSManagedObject *object in managedObjects) {
        if (![object isFault]) {
            [items addObject:object];
        } else {
            NSLog(@"Error object is a fault: %@", object);
        }
    }
        completion(items,nil);
    }
    else {
        completion(nil, error);
    }
    

}

- (void)postItems:(NSSet *)items
         forClass:(Class)class
           completion:(postRequestCompletion)completion {
    if (items && items.count > 0) {
        __block BOOL groupSUCCESS = YES;
        NSMutableSet *sentItems = [NSMutableSet new];
        dispatch_group_t group = dispatch_group_create();
        NSString *path = [self pathForClass:class];
        NSDictionary *params = [NSDictionary new];
        for (NSManagedObject *itemToPost in items) {
            dispatch_group_enter(group);
            
            params = [self paramsForClass:class forItem:itemToPost];
            
            [[RKObjectManager sharedManager] postObject:itemToPost
                                                   path:path
                                             parameters:params
                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                    if ([NSStringFromClass(class) isEqualToString:@"Location"]) {
                                                        Location *sentLocation = (Location *)itemToPost;
                                                        if (!sentLocation.routeId) {
                                                            NSLog (@" no route id");
                                                            if (sentLocation.timestamp) {
                                                                sentLocation.routeId = self.routeID;
                                                            }
                                                        }
                                                    }
                                                    [sentItems addObject:itemToPost];
                                                    dispatch_group_leave(group);
                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                    NSLog(@"Error: %@", [error debugDescription]);
                                                    groupSUCCESS = NO;
                                                    dispatch_group_leave(group);
                                                }];
            
        }
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            if (completion) {
                completion(sentItems, groupSUCCESS);
            }
        });
    } else if (completion) {
        completion (nil, NO);
    }
}

- (NSString *)pathForClass:(Class)class {
    if ([NSStringFromClass(class) isEqualToString:@"Location"]) {
        return @"classes/LocationObject";
    } else if ([NSStringFromClass(class) isEqualToString:@"Route"]) {
        return @"classes/Route";
    } else
    return @"";
}

- (NSDictionary *)paramsForClass:(Class)class forItem:(NSManagedObject *)item {
    if ([NSStringFromClass(class) isEqualToString:@"Location"]) {
        Location *location = (Location *)item;
        return @{ @"timestamp" : @{@"__type" : @"Date", @"iso" : RKStringFromDate(location.timestamp)}};
    } else if ([NSStringFromClass(class) isEqualToString:@"Route"]) {
        Route *route = (Route *)item;
        return @{ @"startTime" : @{@"__type" : @"Date", @"iso" : RKStringFromDate(route.startTime)},
                  @"endTime" : @{@"__type" : @"Date", @"iso" : RKStringFromDate(route.endTime)}
                  };
    } else
        return nil;
}

- (void)destroyItems:(NSSet *)items {
    for (NSManagedObject *itemToDestroy in items) {
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:itemToDestroy];
    }
    NSError *error;
    if ([[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext save:&error] == NO) {
        @throw [NSException exceptionWithName:@"Core Data Save" reason:[@"Failed to save resources due to error: " stringByAppendingString:[error debugDescription]] userInfo:nil];
    }
}

- (void)sendItem:(NSTimer *)timer {
    [self fetchItems:[timer userInfo] withCompletionBlock:^(NSSet *items, NSError *error) {
        if (!error) {
            if (items.count > NUMBER_OF_ITEMS) {
                NSArray *array = [[items allObjects] subarrayWithRange:NSMakeRange(0, NUMBER_OF_ITEMS)];
                items = [[NSSet alloc] initWithArray:array];
            }
            
            if (items.count > 0) {
                [self postItems:items forClass:[timer userInfo] completion:^(NSSet *sentItems, BOOL success) {
                    [self destroyItems:sentItems];
                }];
            }
        }

    }];
}

- (void)saveLocationInCoreData:(CLLocation *)location{
    Location *loc = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext];
    loc.latitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    loc.longitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    loc.verAccuracy = [NSString stringWithFormat:@"%f",location.verticalAccuracy];
    loc.horAccuracy = [NSString stringWithFormat:@"%f",location.horizontalAccuracy];
    loc.timestamp =   location.timestamp;
    loc.uuid = self.deviceUUID;
    loc.routeId = self.routeID;
}

- (void)setResponseAndRequestDescriptors {
    RKObjectManager *manager = [RKObjectManager sharedManager];
    manager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKEntityMapping *locationMapping = [RKEntityMapping mappingForEntityForName:@"Location" inManagedObjectStore:[RKManagedObjectStore defaultStore]];
    RKEntityMapping *routeMapping = [RKEntityMapping mappingForEntityForName:@"Route" inManagedObjectStore:[RKManagedObjectStore defaultStore]];
    
    [routeMapping addAttributeMappingsFromArray:@[@"routeId", @"uuid", @"startTime", @"endTime"]];
    
    [locationMapping addAttributeMappingsFromDictionary:@{
                                                          @"horAccuracy" : @"horAccuracy",
                                                          @"latitude"    : @"latitude",
                                                          @"longitude"   : @"longitude",
                                                          @"timestamp"   : @"timestamp",
                                                          @"verAccuracy" : @"verAccuracy",
                                                    
                                                          @"uuid" : @"uuid",
                                                          @"routeId" : @"routeId"
                                                          }];
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    
    RKResponseDescriptor *locationResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:@"classes/LocationObject"
                                                                                           keyPath:nil
                                                                                       statusCodes:statusCodes];
    
    RKRequestDescriptor *locationRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[locationMapping inverseMapping]
                                                                                   objectClass:[Location class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodAny];
    
    RKResponseDescriptor *routeResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:routeMapping
                                                                                                    method:RKRequestMethodAny
                                                                                               pathPattern:@"classes/Route"
                                                                                                   keyPath:nil
                                                                                               statusCodes:statusCodes];
    
    RKRequestDescriptor *routeRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[routeMapping inverseMapping]
                                                                                           objectClass:[Route class]
                                                                                           rootKeyPath:nil
                                                                                                method:RKRequestMethodAny];

    [manager addRequestDescriptorsFromArray:@[routeRequestDescriptor, locationRequestDescriptor]];
    [manager addResponseDescriptorsFromArray:@[locationResponseDescriptor, routeResponseDescriptor]];
}

- (void)setMapView:(MKMapView *)mapView {
    _mapView = mapView;
    self.mapView.delegate = self;
}

- (void)setLocation:(CLLocation *)location {
    _location = location;
    [self.mapView removeAnnotations:self.mapView.annotations];
    //NSLog(@"Locality : %@, Sublocality : %@", self.placemark.locality, self.placemark.subLocality);
    self.annotation = [[HCSAnnotation alloc]initWithLocation:location.coordinate title:self.placemark.locality subtitle:self.placemark.subLocality];
    [self.mapView addAnnotation:self.annotation];
    self.zoomIn = YES;
    [self zoomInZoomOut];
}

- (void)zoomInZoomOut {
    MKCoordinateRegion region;
    region.center = self.location.coordinate;
    if (self.zoomIn) {
        region.span = MKCoordinateSpanMake(0.1, 0.1);
        self.zoomIn = NO;
    } else {
        region.span = MKCoordinateSpanMake(25.0, 25.0);
        self.zoomIn = YES;
    }
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[HCSAnnotation class]]) {
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (!annotationView) {
             annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.canShowCallout = YES;
            UIButton *disclosureButton = [[UIButton alloc]init];
            [disclosureButton setBackgroundImage:[UIImage imageNamed:@"disclosure"] forState:UIControlStateNormal];
            [disclosureButton sizeToFit];
            annotationView.rightCalloutAccessoryView = disclosureButton;
        }
        annotationView.annotation = annotation;
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    LocationDetailViewController *locationDetailViewController = [[LocationDetailViewController alloc] initWithStyle:UITableViewStyleGrouped];
    locationDetailViewController.location = self.location;
    [self.navigationController pushViewController:locationDetailViewController animated:YES];
}

- (CLGeocoder *)geocoder {
    if (!_geocoder) {
        _geocoder = [[CLGeocoder alloc]init];
    }
    return _geocoder;
}


@end
